import json
import boto3
import os

s3 = boto3.resource(u's3')

def lambda_handler(event, context):
    bucketName = event['s3_bucket']
    bucket = s3.Bucket(bucketName)
    key_list = []

    for file in bucket.objects.all():
        key_list.append(file.key)

    return {
        "files": key_list
    }
